package prestashoptest.datatypes;

import java.util.Comparator;

public class CartLine implements Comparable<CartLine> {

    final String product;
    final int quantity;
    final String dimension;
    final String size;
    final String color;

    public CartLine(final String product,
                    final int quantity,
                    final String dimension,
                    final String size,
                    final String color) {
        super();
        this.product = product;
        this.quantity = quantity;
        this.dimension = dimension;
        this.size = size;
        this.color = color;
    }

    @Override
    public String toString() {
        return "[product=" + product
                + ", quantity=" + quantity 
                + ", dimension=" + dimension 
                + ", size="    + size
                + ", color=" + color + "]";
    }

    public String getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }
    
    public String getDimension() {
        return dimension;
    }

    public String getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    private static Comparator<String> stringComparator = Comparator.nullsFirst(String::compareTo);
    private static Comparator<CartLine> metadataComparator = Comparator.comparing(CartLine::getProduct)
                                                                       .thenComparing(CartLine::getQuantity)
                                                                       .thenComparing(CartLine::getDimension, stringComparator)
                                                                       .thenComparing(CartLine::getSize, stringComparator)
                                                                       .thenComparing(CartLine::getColor, stringComparator);

    @Override
    public int compareTo(final CartLine other) {
        return metadataComparator.compare(this, other);
    }
}