package prestashoptest.datatypes;

/**
 * @Gender of a person
 */
public enum Gender {

    MALE("M"),
    FEMALE("F"),
    UNDEFINED("U");

    private final String code;

    private Gender(final String code) {
        this.code = code;
    }

    public static Gender ofCode(final String code) {
       for (Gender gender : Gender.values()) {
           if (gender.code.equals(code)) {
               return gender;
           }
       }
       throw new IllegalArgumentException("\"" + code + "\" is not a Gender code");
    }
}
