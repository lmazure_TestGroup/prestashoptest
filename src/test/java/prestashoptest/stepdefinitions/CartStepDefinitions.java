package prestashoptest.stepdefinitions;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import prestashoptest.datatypes.CartLine;
import prestashoptest.pageobjects.CartPageObject;
import prestashoptest.pageobjects.CatalogPageObject;
import prestashoptest.pageobjects.HomePageObject;
import prestashoptest.pageobjects.ProductPageObject;

public class CartStepDefinitions {

    /**
     * go to a category
     *
     * the current page must be the home page
     */
    @When("I navigate to category {string}")
    public void navigateToCategory(final String category) {
        final HomePageObject homePage = new HomePageObject();
        homePage.assertIsCurrent();
        homePage.selectCategory(category);
        final CatalogPageObject catalogPage = new CatalogPageObject();
        catalogPage.assertIsCurrent();
    }

    /**
     * go to a subcategory
     *
     * the current page must be the home page
     */
    @When("I navigate to subcategory {string}")
    public void navigateToSubcategory(final String subcategory) {
        final HomePageObject homePage = new HomePageObject();
        homePage.assertIsCurrent();
        homePage.selectSubcategory(subcategory);
        final CatalogPageObject catalogPage = new CatalogPageObject();
        catalogPage.assertIsCurrent();
    }

    /**
     * go to a product
     *
     * the current page must be the catalog page
     */
    @When("I navigate to product {string}")
    public void navigateToProduct(final String product) {
        final CatalogPageObject catalogPage = new CatalogPageObject();
        catalogPage.assertIsCurrent();
        catalogPage.selectProduct(product);
        final ProductPageObject productPage = new ProductPageObject();
        productPage.assertIsCurrent();
    }

    /**
     * add the current product to cart
     *
     * the current page must be the product page
     */
    @When("I add to cart")
    public void addToCartAndClosePopup() {
        final ProductPageObject productPage = new ProductPageObject();
        productPage.assertIsCurrent();
        productPage.addToCart();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        productPage.closePopup();
    }

    /**
     * verify that the cart data is equal to the given lines
     */
    @Then("The cart contains")
    public void assertCartContent(final DataTable cart) {

        // get the effective cart content
        final CartPageObject cartPage = new CartPageObject();
        cartPage.goTo();
        final List<CartLine> effectiveCartLines = cartPage.getContent();

        // get the expected cart content (from the Gherkin dataset)
        final List<CartLine> expectedCartLines = cart.asLists(String.class)
                                                     .stream()
                                                     .skip(1)
                                                     .map(strings -> new CartLine(strings.get(0),
                                                                                  Integer.parseInt(strings.get(1)),
                                                                                  strings.get(2),
                                                                                  strings.get(3),
                                                                                  strings.get(4)))
                                                     .sorted()
                                                     .collect(Collectors.toList());

        // assert that both are equal
        final String errorDescription = "expected cart:\n"
                                        + expectedCartLines.stream().map(CartLine::toString).collect(Collectors.joining("\n"))
                                        + "\neffective cart:\n"
                                        + effectiveCartLines.stream().map(CartLine::toString).collect(Collectors.joining("\n"))
                                        + "\n";

        Assertions.assertEquals(expectedCartLines.size(),
                                effectiveCartLines.size(),
                                "The cart does not contain the expected number of lines\n" + errorDescription);
        for (int i = 0; i < expectedCartLines.size(); i++) {
            Assertions.assertEquals(expectedCartLines.get(i).getProduct(),
                                    effectiveCartLines.get(i).getProduct(),
                                    "The line of the cart does not contain the expected product\n" + errorDescription);
            Assertions.assertEquals(expectedCartLines.get(i).getQuantity(),
                                    effectiveCartLines.get(i).getQuantity(),
                                    "The line of the cart does not contain the expected number\n" + errorDescription);
            Assertions.assertEquals(expectedCartLines.get(i).getDimension(),
                                    effectiveCartLines.get(i).getDimension(),
                                    "The line of the cart does not contain the expected dimension\n" + errorDescription);
            Assertions.assertEquals(expectedCartLines.get(i).getSize(),
                                    effectiveCartLines.get(i).getSize(),
                                    "The line of the cart does not contain the expected size\n" + errorDescription);
            Assertions.assertEquals(expectedCartLines.get(i).getColor(),
                                    effectiveCartLines.get(i).getColor(),
                                    "The line of the cart does not contain the expected color\n" + errorDescription);
        }
    }
}
