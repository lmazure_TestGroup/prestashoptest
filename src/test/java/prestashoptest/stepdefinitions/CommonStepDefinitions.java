package prestashoptest.stepdefinitions;

import io.cucumber.java.en.When;
import prestashoptest.pageobjects.SignInPageObject;

public class CommonStepDefinitions {

    /**
     * go to the Sign In page
     */
    @When("I am on the SignIn page")
    public void displaySignInPage() {
        final SignInPageObject signInPage = new SignInPageObject();
        signInPage.goTo();
    }
}
