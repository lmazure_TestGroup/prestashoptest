package prestashoptest.stepdefinitions;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Assertions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import prestashoptest.datatypes.Gender;
import prestashoptest.helpers.StringsHelper;
import prestashoptest.pageobjects.AccountCreationPageObject;
import prestashoptest.pageobjects.HomePageObject;
import prestashoptest.pageobjects.IdentityPageObject;
import prestashoptest.pageobjects.SignInPageObject;
import prestashoptest.seleniumtools.PageObjectBase;

public class AccountStepDefinitions {

    /**
     * create a temporary user and keep him/her logged in
     */
    @Given("I am logged in")
    public void createTemporaryUser() {
    	final String id = StringsHelper.generateRandomId();
        final AccountCreationPageObject newAccountPage = new AccountCreationPageObject();
        newAccountPage.goTo();
        newAccountPage.fillNewAccountFields(Gender.UNDEFINED,
                                            "first. " + id,
                                            "last. " + id,
                                            id,
                                            id + "@example.com",
                                            LocalDate.of(2000, 1, 1),
                                            true,
                                            true,
                                            true,
                                            true);
        final HomePageObject homePage = new HomePageObject();
        homePage.assertIsCurrent();
    }

    /**
     * go to the Home page
     */
    @When("I am on the Home page")
    public void displayHomePage() {
        final HomePageObject homePage = new HomePageObject();
        homePage.goTo();
    }


    /**
     * go to the Account Creation page
     */
    @When("I am on the AccountCreation page")
    public void displayNewAccountPage() {
        final AccountCreationPageObject newAccountPage = new AccountCreationPageObject();
        newAccountPage.goTo();
    }

    /**
     * fill the fields of the Account Creation page and submit the form
     *
     * the current page must be the Account Creation page
     */
    @When("I fill AccountCreation fields with gender={string} firstName={string} lastName={string} password={string} email={string} birthDate={string} acceptPartnerOffers={string} acceptPrivacyPolicy={string} acceptNewsletter={string} acceptGdpr={string} and submit")
    public void fillAccountCreationFieldsAndSubmit(final String genderCode,
                                                   final String firstName,
                                                   final String lastName,
                                                   final String password,
                                                   final String email,
                                                   final String birthDate,
                                                   final String acceptPartnerOffers,
                                                   final String acceptPrivacyPolicy,
                                                   final String acceptNewsletter,
                                                   final String acceptGdpr) {
        final AccountCreationPageObject newAccountPage = new AccountCreationPageObject();
        newAccountPage.assertIsCurrent();
        newAccountPage.fillNewAccountFields(Gender.ofCode(genderCode),
                                            firstName,
                                            lastName,
                                            password,
                                            email,
                                            LocalDate.parse(birthDate),
                                            StringsHelper.convertYesNoIntoBoolean(acceptPartnerOffers),
                                            StringsHelper.convertYesNoIntoBoolean(acceptPrivacyPolicy),
                                            StringsHelper.convertYesNoIntoBoolean(acceptNewsletter),
                                            StringsHelper.convertYesNoIntoBoolean(acceptGdpr));
    }

    /**
     * sign out
     *
     * the current page must be the home page
     */
    @When("I sign out")
    public void signOutFromHomePage() {
        final HomePageObject homePage = new HomePageObject();
        homePage.assertIsCurrent();
        homePage.signOut();
    }

    /**
     * fill the fields of the Sign In page and submit the form
     *
     * the current page must be the SignIn page
     */
    @When("I fill SignIn fields with email={string} password={string} and submit")
    public void fillSignInFieldsAndSubmit(final String email,
                                          final String password) {
        final SignInPageObject signInPage = new SignInPageObject();
        signInPage.assertIsCurrent();
        signInPage.signIn(email, password);
    }

    /**
     * verify that signing in with the given parameters is successful
     */
    @Then("I can successfully sign in with email={string} password={string}")
    public void assertSuccessfulSignIn(final String email,
                                       final String password) {
        final SignInPageObject signInPage = new SignInPageObject();
        signInPage.goTo();
        signInPage.signIn(email, password);
        final HomePageObject homePage = new HomePageObject();
        homePage.assertIsCurrent();
    }

    /**
     * verify that the personal data is equal to the given parameters
     */
    @Then("My personal information is gender={string} firstName={string} lastName={string} email={string} birthDate={string} acceptPartnerOffers={string} acceptPrivacyPolicy={string} acceptNewsletter={string} acceptGdpr={string}")
    public void assertPersonnalInformation(final String genderCode,
                                           final String firstName,
                                           final String lastName,
                                           final String email,
                                           final String birthDate,
                                           final String acceptPartnerOffers,
                                           final String acceptPrivacyPolicy,
                                           final String acceptNewsletter,
                                           final String acceptGdpr) {
        final IdentityPageObject identityPage = new IdentityPageObject();
        identityPage.goTo();
        Assertions.assertEquals(Gender.ofCode(genderCode),
                                identityPage.getGender(),
                                "The effective gender is not the expected one");
        Assertions.assertEquals(firstName,
                                identityPage.getFirstName(),
                                "The effective first name is not the expected one");
        Assertions.assertEquals(lastName,
                                identityPage.getLastName(),
                                "The effective last name is not the expected one");
        Assertions.assertEquals(email,
                                identityPage.getEmail(),
                                "The effective email is not the expected one");
        Assertions.assertEquals(LocalDate.parse(birthDate),
                                identityPage.getBirthDate(),
                                "The effective birth date is not the expected one");
        Assertions.assertEquals(StringsHelper.convertYesNoIntoBoolean(acceptPartnerOffers),
                                identityPage.doesAcceptPartnerOffers(),
                                "The effective acceptPartnerOffers is not the expected one");
        Assertions.assertEquals(StringsHelper.convertYesNoIntoBoolean(acceptPrivacyPolicy),
                                identityPage.doesAcceptPrivacyPolicy(),
                                "The effective acceptPrivacyPolicy is not the expected one");
        Assertions.assertEquals(StringsHelper.convertYesNoIntoBoolean(acceptNewsletter),
                                identityPage.doesAcceptNewsletter(),
                                "The effective acceptNewsletter is not the expected one");
        Assertions.assertEquals(StringsHelper.convertYesNoIntoBoolean(acceptGdpr),
                                identityPage.doesAcceptGdpr(),
                                "The effective acceptGdpr is not the expected one");
    }

    /**
     * verify that the given message is currently displayed as an error message
     */
    @Then("The error message {string} is displayed")
    public void assertErrorMessageIsDisplayed(final String errorMessage) {
        final List<String> errorMessages = PageObjectBase.getErrorMessages();
        final String failedAssertDescription = "Error message \"" + errorMessage + "\" is not in list of current error messages: " + (errorMessage.isEmpty() ? "none"
                                                                                                                                                             : ("\"" + String.join("\",\"", errorMessages) + "\"")); // TODO cleanup this crap
        Assertions.assertTrue(errorMessages.contains(errorMessage), failedAssertDescription);
    }
}
