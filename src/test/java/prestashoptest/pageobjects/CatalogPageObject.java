package prestashoptest.pageobjects;

import java.util.regex.Pattern;

import prestashoptest.seleniumtools.PageObjectBase;

public class CatalogPageObject extends PageObjectBase {

    public CatalogPageObject() {
        super(Pattern.compile("\\d+-\\w+"));
    }

    public void selectProduct(final String product) {
        clickElement(SelectBy.LINKTEXT, product);
    }
}
