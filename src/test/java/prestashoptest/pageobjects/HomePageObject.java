package prestashoptest.pageobjects;

import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Home page
 */
public class HomePageObject extends PageObjectBase {

    public HomePageObject() {
        super("");
    }

    public void signOut() {
        clickElement(SelectBy.LINKTEXTEXTRACT, "Déconnexion");
    }

    public void selectCategory(final String category) {
        clickElement(SelectBy.XPATH, "//a[contains(text(),'" + category + "')]");
    }

    public void selectSubcategory(final String subcategory) {
        final String subcategoryXPath = "//a[normalize-space(text())='" + subcategory + "']";
        hoverElement(SelectBy.XPATH, subcategoryXPath + "/../../../preceding-sibling::a"); // TODO XPATH too complex, this will not be robust
        clickElement(SelectBy.XPATH, subcategoryXPath);
    }
}
