package prestashoptest.pageobjects;

import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Login Page
 */
public class SignInPageObject extends PageObjectBase {

    public SignInPageObject() {
        super("connexion");
    }

    /**
     * Initiate the login with the specified user mail and password
     *
     * @param email
     * @param password
     */
    public void signIn(final String email,
                       final String password) {
        fillEmail(email);
        fillPassword(password);
        submitForm();
    }

    public void fillEmail(final String email) {
        setFieldValue(SelectBy.ID, "field-email", email);
    }

    public void fillPassword(final String password) {
        setFieldValue(SelectBy.ID, "field-password", password);
    }

    public void submitForm() {
        clickElement(SelectBy.ID, "submit-login");
    }
}
