package prestashoptest.pageobjects;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import prestashoptest.datatypes.Gender;
import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Personal information page
 *
 */
public class IdentityPageObject extends PageObjectBase {

    private final static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("d/MM/yyyy");

    public IdentityPageObject() {
        super("identite");
    }

    public Gender getGender() {
        if (isCheckBoxSelected(SelectBy.ID, "field-id_gender-1")) {
            return Gender.MALE;
        }
        if (isCheckBoxSelected(SelectBy.ID, "field-id_gender-2")) {
            return Gender.FEMALE;
        }
        return Gender.UNDEFINED;
    }

    public String getFirstName() {
        return getFieldValue(SelectBy.ID, "field-firstname");
    }

    public String getLastName() {
        return getFieldValue(SelectBy.ID, "field-lastname");
    }

    public String getEmail() {
        return getFieldValue(SelectBy.ID, "field-email");
    }

    public LocalDate getBirthDate() {
        final String birthDateStr = getFieldValue(SelectBy.ID, "field-birthday");
        return LocalDate.parse(birthDateStr, DATE_FORMATTER);
    }

    public boolean doesAcceptPartnerOffers() {
        return isCheckBoxSelected(SelectBy.NAME, "optin");
    }

    public boolean doesAcceptPrivacyPolicy() {
        return isCheckBoxSelected(SelectBy.NAME, "customer_privacy");
    }

    public boolean doesAcceptNewsletter() {
        return isCheckBoxSelected(SelectBy.NAME, "newsletter");
    }

    public boolean doesAcceptGdpr() {
        return isCheckBoxSelected(SelectBy.NAME, "psgdpr");
    }
}
