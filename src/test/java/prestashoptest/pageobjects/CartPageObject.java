package prestashoptest.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import prestashoptest.datatypes.CartLine;
import prestashoptest.seleniumtools.PageObjectBase;

public class CartPageObject  extends PageObjectBase {

    public CartPageObject() {
        super("panier?action=show");
    }

    public List<CartLine> getContent() {
        final List<WebElement> items =  SelectBy.CLASS.findElements(getDriver(), "cart-item");
        final List<CartLine> list = new ArrayList<CartLine>(items.size());
        for (final WebElement item: items) {
            final WebElement productElem = item.findElement(By.className("label"));
            final String product = productElem.getText();
            final WebElement quantityElem = item.findElement(By.name("product-quantity-spin"));
            final int quantity = Integer.parseInt(quantityElem.getAttribute("value"));
            final List<WebElement> dimensionElem = item.findElements(By.className("dimension"));
            final String dimension = dimensionElem.isEmpty() ? null : dimensionElem.get(0).findElement(By.className("value")).getText();  
            final List<WebElement> sizeElem = item.findElements(By.className("taille"));
            final String size = sizeElem.isEmpty() ? null : sizeElem.get(0).findElement(By.className("value")).getText();  
            final List<WebElement> colorElem = item.findElements(By.className("couleur"));
            final String color = colorElem.isEmpty() ? null : colorElem.get(0).findElement(By.className("value")).getText();  
            list.add(new CartLine(product, quantity, dimension, size, color));
        }
        return list;
    }
}
