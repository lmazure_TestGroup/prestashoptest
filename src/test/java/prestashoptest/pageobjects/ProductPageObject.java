package prestashoptest.pageobjects;

import java.util.regex.Pattern;

import prestashoptest.seleniumtools.PageObjectBase;

public class ProductPageObject extends PageObjectBase {

    public ProductPageObject() {
        super(Pattern.compile("\\w+/\\d+-(\\d+-)?[-\\w]+\\.html"));
    }

    public void addToCart() {
        clickElement(SelectBy.CLASS, "add-to-cart");
    }

    public void closePopup() {
        //  close the popup by clicking on the backdrop
        clickElement(SelectBy.ID, "blockcart-modal");
    }
}
