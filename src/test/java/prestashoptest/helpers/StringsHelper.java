package prestashoptest.helpers;

import java.util.Random;

public class StringsHelper {

    public static boolean convertYesNoIntoBoolean(final String str) {
        if (str.equals("yes")) return true;
        if (str.equals("no")) return false;
        throw new IllegalArgumentException("Cannot convert \"" + str + "\" into a boolean, the string should be \"yes\" or \"no\".");
    }
    
    public static String generateRandomId() {
    	final int LENGTH = 16;
    	final Random random = new Random();
    	final StringBuilder builder = new StringBuilder(LENGTH + 1);
    	for (int i = 0; i < LENGTH; i++) {
        	final int r = random.nextInt(25);
    		builder.append((char)('a' + r));
    	}
    	return builder.toString();
    }
}
