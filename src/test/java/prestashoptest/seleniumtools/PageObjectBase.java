package prestashoptest.seleniumtools;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.By.ByLinkText;
import org.openqa.selenium.By.ByName;
import org.openqa.selenium.By.ByPartialLinkText;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Base class for all the page objects
 */
public class PageObjectBase {

    private final String url;
    private final Pattern urlRegex;

    private static Browser browser;
    private static String host;

    private static final Duration TIMEOUT = Duration.ofSeconds(10);

    /**
     * Constructor for page having a constant URL
     *
     * @param url
     */
    protected PageObjectBase(final String url) {
        Objects.requireNonNull(PageObjectBase.host);
        Objects.requireNonNull(PageObjectBase.browser);
        this.url = url;
        this.urlRegex = null;
    }

    /**
     * Constructor for page not having a constant URL
     *
     * @param urlRegex
     */
    protected PageObjectBase(final Pattern urlRegex) {
        Objects.requireNonNull(PageObjectBase.host);
        Objects.requireNonNull(PageObjectBase.browser);
        this.url = null;
        this.urlRegex = urlRegex;
    }

    public static void setHost(final String host) {
        PageObjectBase.host = host;
    }

    public static void setBrowser(final Browser browser) {
        PageObjectBase.browser = browser;
    }

    public static void quit() {
        PageObjectBase.browser.quit();
        PageObjectBase.browser = null;
        PageObjectBase.host = null;
    }

    public static byte[] getScreenshot() {
        return ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES);
    }

    public void goTo() {
        if (!hasConstantUrl()) {
            throw new IllegalStateException("The current Page Object does not support goTo (it has no constant URL)");
        }
        getDriver().get(host + "/" + this.url);
    }

    public void assertIsRedirectedTo() {
        new WebDriverWait(getDriver(), TIMEOUT).until(driver -> currentUrlIsExpected(driver.getCurrentUrl()));
    }

    public void assertIsCurrent() {
        final String currentUrl = getDriver().getCurrentUrl();
        Assertions.assertTrue(currentUrlIsExpected(currentUrl),
                              "current URL (" + currentUrl
                              + ") does not match the expected one (" + host + "/"
                              + (hasConstantUrl() ? this.url : this.urlRegex) + ")");
    }

    protected static WebDriver getDriver() { // TODO à remettre en private
        return PageObjectBase.browser.getDriver();
    }

    private boolean hasConstantUrl() {
        return (this.url != null);
    }

    private boolean currentUrlIsExpected(final String currentUrl) {
        final String currentUrlWithParams = currentUrl.replaceFirst("[&#].*$", "");
        if (hasConstantUrl()) {
            return currentUrlWithParams.equals(host + "/" + this.url);
        } else {
            if (!currentUrlWithParams.startsWith(host + "/")) return false;
            final String pageUrlComponent = currentUrlWithParams.substring(host.length() + 1);
            final Matcher matcher = this.urlRegex.matcher(pageUrlComponent);
            return matcher.matches();
        }
    }

    protected void setFieldValue(final SelectBy by,
                                 final String fieldKey,
                                 final String value) {
        final WebElement element = by.findElement(getDriver(), fieldKey);
        element.clear();
        element.sendKeys(value);
    }

    protected String getFieldValue(final SelectBy by,
                                   final String fieldKey) {
        return by.findElement(getDriver(), fieldKey).getAttribute("value");
    }

    protected void clickElement(final SelectBy by,
                                final String fieldKey) {
        by.findElement(getDriver(), fieldKey).click();
    }


    protected void hoverElement(final SelectBy by,
                                final String fieldKey) {
        final Actions actions = new Actions(getDriver());
        final WebElement element = by.findElement(getDriver(), fieldKey);
        actions.moveToElement(element).perform();
    }

    protected boolean isCheckBoxSelected(final SelectBy by,
                                         final String checkBoxKey) {
        return by.findElement(getDriver(), checkBoxKey).isSelected();
    }

    public static List<String> getErrorMessages() {
        return SelectBy.CLASS.findElements(getDriver(), "alert-danger").stream()
                             .map(e -> e.getText())
                             .collect(Collectors.toList());
    }

    public enum SelectBy {

        ID(s -> new ById(s)),
        NAME(s -> new ByName(s)),
        XPATH(s -> new ByXPath(s)),
        CLASS(s -> new ByClassName(s)),
        LINKTEXT(s -> new ByLinkText(s)),
        LINKTEXTEXTRACT(s -> new ByPartialLinkText(s));

        private final Function<String, By> selector;

        private SelectBy(final Function<String, By> selector) {
            this.selector =selector;
        }

        private WebElement findElement(final WebDriver driver,
                                       final String key) {
            return getDriver().findElement(this.selector.apply(key));
        }

        public List<WebElement> findElements(final WebDriver driver, // TODO à remettre en private
                                             final String key) {
            return getDriver().findElements(this.selector.apply(key));
       }
}

    public enum Browser {

        CHROME(() -> new ChromeDriver()),
        FIREFOX(() -> new FirefoxDriver()),
        EDGE(() -> new EdgeDriver());

        private final Supplier<WebDriver> driverBuilder;
        private WebDriver driver;

        private Browser(final Supplier<WebDriver> driverBuilder) {
            this.driverBuilder = driverBuilder;
        }

        private WebDriver getDriver() {
            // we perform a late creation of the driver so the user does not need all possible drivers on his/her machine, s/he just needs the ones s/he uses
            if (this.driver == null) {
                this.driver = driverBuilder.get();
            }
            return this.driver;
        }

        private void quit() {
            this.driver.quit();
            this.driver = null;
        }
    }
}
