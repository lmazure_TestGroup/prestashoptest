# Automation priority: null
# Test case importance: Very high
# language: en
Feature: Cannot log with incorrect password

	Scenario: Cannot log with incorrect password
		When I am on the AccountCreation page
		When I fill AccountCreation fields with gender="F" firstName="Alice" lastName="Noël" password="police" email="alice@bob.com" birthDate="1970-01-01" acceptPartnerOffers="yes" acceptPrivacyPolicy="yes" acceptNewsletter="yes" acceptGdpr="yes" and submit
		When I am on the Home page
		When I sign out
		When I am on the SignIn page
		When I fill SignIn fields with email="alice@bob.com" password="poluce" and submit
		Then The error message "Échec d'authentification" is displayed