# Automation priority: null
# Test case importance: Low
# language: en
Feature: Standard account creation

	Scenario Outline: Standard account creation
		When I am on the AccountCreation page
		And I fill AccountCreation fields with gender=<gender> firstName=<first> lastName=<last> password=<password> email=<mail> birthDate=<birth> acceptPartnerOffers=<offers> acceptPrivacyPolicy=<privacy> acceptNewsletter=<news> acceptGdpr=<gpdr> and submit
		And I am on the Home page
		And I sign out
		Then I can successfully sign in with email=<mail> password=<password>
		And My personal information is gender=<gender> firstName=<first> lastName=<last> email=<mail> birthDate=<birth> acceptPartnerOffers=<offers> acceptPrivacyPolicy="no" acceptNewsletter=<news> acceptGdpr="no"

		@01
		Examples:
		| birth | first | gender | gpdr | last | mail | news | offers | password | privacy |
		| "2022-01-02" | "John" | "M" | "yes" | "Doe" | "jdoe@example.com" | "yes" | "no" | "mypassword" | "yes" |

		@02
		Examples:
		| birth | first | gender | gpdr | last | mail | news | offers | password | privacy |
		| "2022-01-02" | "Jane" | "F" | "yes" | "Doe" | "jane.doe@oo.com" | "yes" | "yes" | "123&µ" | "yes" |

		@03
		Examples:
		| birth | first | gender | gpdr | last | mail | news | offers | password | privacy |
		| "2022-02-10" | "Camille" | "U" | "yes" | "Bertillon" | "cb@gmail.com" | "no" | "yes" | "£→µ$¤11" | "yes" |

		@04
		Examples:
		| birth | first | gender | gpdr | last | mail | news | offers | password | privacy |
		| "2022-02-10" | "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" | "U" | "yes" | "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" | "012345678901234567890123456789012345678901234567890123456789@oo.com" | "yes" | "yes" | "abcdefghijklmnopqrstuvwxyz" | "yes" |