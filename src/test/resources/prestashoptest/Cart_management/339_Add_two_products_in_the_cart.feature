# Automation priority: 10
# Test case importance: Low
# language: en
Feature: Add two products in the cart

	Scenario: Add two products in the cart
		Given I am logged in
		When I am on the Home page
		When I navigate to category "Art"
		When I navigate to product "Affiche Encadrée The Best..."
		When I add to cart
		When I am on the Home page
		When I navigate to category "Art"
		When I navigate to product "Illustration Vectorielle..."
		When I add to cart
		Then The cart contains
			      | Product                                  | Number | Dimension | Size | Color |
			      | Affiche encadrée The best is yet to come |      1 | 40x60cm   |      |       |
			      | Illustration vectorielle Renard          |      1 |           |      |       |
			