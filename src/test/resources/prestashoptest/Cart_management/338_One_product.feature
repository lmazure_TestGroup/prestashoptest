# Automation priority: null
# Test case importance: Low
# language: en
Feature: One product

	Scenario: One product
		Given I am logged in
		When I am on the Home page
		When I navigate to category "Art"
		When I navigate to product "Affiche Encadrée The Best..."
		When I add to cart
		Then The cart contains
			      | Product                                  | Number | Dimension | Size | Color |
			      | Affiche encadrée The best is yet to come |      1 | 40x60cm   |      |       |